<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
<title><![CDATA[EA2FBM]]></title>
<description><![CDATA[EA2FBM]]></description>
<link>https://ea2fbmemacs.gitlab.io/</link>
<lastBuildDate>Tue, 31 Oct 2023 07:17:41 +0100</lastBuildDate>
<item>
  <title><![CDATA[Agenda en Org-mode]]></title>
  <description><![CDATA[
<p>
Una de las grandes funcionalidades que nos proporciona Emacs y Org-Mode es el
modo Agenda. Con el modo Agenda vamos a poder programar nuestro día a día de una
manera muy productiva.
</p>

<p>
La idea general es disponer de varios archivos .org de diferente temática donde
vayamos reflejando nuestras tareas pendientes.
</p>

<p>
Para ello en nuestro archivo de configuración de Emacs, configuraremos nuestra
Lista Todo Global, algo así:
</p>

<pre class="example" id="orgdaa1a85">

(setq org-agenda-files (list "/home/luis/Documentos/ORG-FILES/NotasCasa.org&gt;
                         "/home/luis/Documentos/ORG-FILES/BLOG/NotasUni.org&gt;
                        "/home/luis/Documentos/ORG-FILES/BLOG/tareas_globales.org&gt;
                        "/home/luis/Documentos/ORG-FILES/BLOG/Org_Blog.org&gt;
                       "/home/luis/Documentos/ORG-FILES/BLOG/ApuntesClase.org&gt;
</pre>

<p>
Podríamos asignar también un directorio completo donde tener en cuenta todos
los archivos .org alojados.
</p>

<pre class="example" id="org8cabf5c">
(setq org-agenda-files '("~/mis-archivos-org"))
</pre>


<p>
Como opción podríamos añadir diferentes <b>palabras clave</b> con diferentes colores
para hacer más precisa su utilización y no basarnos únicamente en las palabras
clave TODO y DONE.
</p>

<p>
Importante tener en cuenta en la configuración la posición de <b>"|"</b>, ésta nos
delimita las palabras que usamos para dar por completada una tarea.
</p>

<p>
Aquí podéis encontrar los colores tanto para el fondo como para la fuente con
sus correspondientes códigos que soporta Emacs.
</p>

<p>
<a href="http://www.raebear.net/computers/emacs-colors/">http://www.raebear.net/computers/emacs-colors/</a>
</p>

<pre class="example" id="org6350abe">

;;CONFIGURACION ORG MODE-- PALABRAS CLAVE
(setq org-todo-keywords
'((sequence "TODO" "BORRADOR" "TRABAJANDO" "|" "DONE" "PUBLICADA" "POSPUESTO")))


;;CONFIGURACION ORG MODE-- COLORES PALABRAS CLAVE
(setq org-todo-keyword-faces
'(
("TODO"   . (:background "firebrick2" :weight bold))
("BORRADOR"  . (:foreground:"gray0" :background "orange red" :weight bold))
("DONE"   . (:foreground "gray0":background "green2" :weight bold))
("TRABAJANDO"   . (:foreground "navy" :background "azure2" :weight bold))
("PUBLICADA"   . (:foreground "gray0":background "green" :weight bold))
("POSPUESTO"  . (:foreground "gray0":background "grey70" :weight bold))
))
</pre>


<p>
Configuraremos también las asignaciones más comunes para la Agenda,
</p>

<p>
Con <b>C-cl</b> creamos un link. Con <b>C-ca</b> llamamos al menú Agenda.
</p>

<p>
Con C-cc nos aparece org-capture, muy útil para tomar notas al vuelo y más adelante
clasificar, lo cerraríamos con <b>C-c C-c</b> volviendo automáticamente al buffer donde
estábamos.
</p>

<pre class="example" id="orga874823">
;; AGENDA
;;-------
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)

</pre>


<p>
<b>Usos más frecuentes con Org-Agenda</b>
</p>

<p>
Con <b>C-c C-s</b> abrimos el calendario, seleccionando la fecha programaremos la tarea
donde estemos.
</p>

<p>
Abriremos Org-Agenda con <b>C-c a</b>, pulsando <b>a</b> nos muestra la agenda semanal, si
invocamos al comando con un prefijo numérico nos mostrará el número de días que
hemos seleccionado.
</p>
<pre class="example" id="org1cb7e8f">
C-u 20 C-c-a a ;;nos mostrará la agenda en los siguientes 20 días
</pre>

<ul class="org-ul">
<li><b>C-c a T</b> Podremos introducir una palabra clave para que sólo se nos muestren
éstas.</li>

<li><b>C-c a m</b> Nos mostrará las tareas con unas determinadas etiquetas.</li>

<li><b>C-c a M</b> Igual que la anterior pero con la condición que esas tareas sean un
TODO.</li>

<li><b>C-c a t</b> Nos mostará todos los TODO pendientes.</li>

<li><b>C-c a s</b> Buscaremos una cadena de texto en todos los items.</li>

<li><b>C-c a n</b> Nos muestra tanto la planificación semanal como los TODO pendientes.</li>
</ul>

<p>
Si qureremos programar unas tareas que se repitan durante un determinado tiempo,(horas, días, meses, años) y que cuando demos por DONE una tarea no nos de por finalizada las siguientes, deberemos configurarlo de esta manera.
#+begin_src
</p>
<div class="taglist"><a href="https://ea2fbmemacs.gitlab.io/tags.html">Categoría</a>: <a href="https://ea2fbmemacs.gitlab.io/tag-org-mode;.html">org-mode;</a> <a href="https://ea2fbmemacs.gitlab.io/tag-emacs.html">emacs</a> </div>]]></description>
  <category><![CDATA[org-mode;]]></category>
  <category><![CDATA[emacs]]></category>
  <link>https://ea2fbmemacs.gitlab.io/posts2023-10-30-agenda-en-org-mode.html</link>
  <guid>https://ea2fbmemacs.gitlab.io/posts2023-10-30-agenda-en-org-mode.html</guid>
  <pubDate>Mon, 30 Oct 2023 07:41:00 +0100</pubDate>
</item>
<item>
  <title><![CDATA[Primeros pasos con Orgzly]]></title>
  <description><![CDATA[
<p>
¡Bienvenido a Orgzly!
</p>

<div id="outline-container-orge7a5409" class="outline-2">
<h2 id="orge7a5409">Notas</h2>
<div class="outline-text-2" id="text-orge7a5409">
</div>
<div id="outline-container-orgdbfc3b2" class="outline-3">
<h3 id="orgdbfc3b2">Haga clic en la nota para abrirla</h3>
</div>
<div id="outline-container-org900b272" class="outline-3">
<h3 id="org900b272">Haga clic y mantenga la nota para seleccionarla</h3>
<div class="outline-text-3" id="text-org900b272">
<p>
Se pueden seleccionar múltiples notas.
</p>
</div>
</div>

<div id="outline-container-org3449a63" class="outline-3">
<h3 id="org3449a63">Arrastre la nota a la derecha para abrir su menú rápido</h3>
<div class="outline-text-3" id="text-org3449a63">
<p>
Arrastre a la izquierda para abrir un menú diferente.
</p>
</div>
</div>

<div id="outline-container-orgde84b97" class="outline-3">
<h3 id="orgde84b97">Plegar una nota que contiene subnotas haciendo clic en el icono a su lado</h3>
<div class="outline-text-3" id="text-orgde84b97">
<p>
Haga clic largo en el icono para desplegar subárbol completo.
</p>
</div>

<div id="outline-container-orgb5a345d" class="outline-4">
<h4 id="orgb5a345d">No hay límite en el número de niveles que se pueden tener</h4>
<div class="outline-text-4" id="text-orgb5a345d">
</div>
<ul class="org-ul">
<li><a id="orgca3d26a"></a>Utilice esto para organizar mejor sus notas<br></li>
</ul>
</div>
</div>
<div id="outline-container-org6295129" class="outline-3">
<h3 id="org6295129">Una nota puede tener etiquetas&#xa0;&#xa0;&#xa0;<span class="tag"><span class="tag1">tag1</span>&#xa0;<span class="tag2">tag2</span></span></h3>
<div class="outline-text-3" id="text-org6295129">
</div>
<div id="outline-container-org73df475" class="outline-4">
<h4 id="org73df475">Las subnotas heredan las etiquetas&#xa0;&#xa0;&#xa0;<span class="tag"><span class="tag3">tag3</span></span></h4>
<div class="outline-text-4" id="text-org73df475">
<p>
Si busca por notas con la etiqueta "etiqueta1" usando la búsqueda "t.etiqueta1", esta nota se incluirá en los resultados de búsqueda.
</p>
</div>
</div>

<div id="outline-container-org3419a00" class="outline-4">
<h4 id="org3419a00">Una nota que tiene la etiqueta <code>ARCHIVE</code> se pone grisácea&#xa0;&#xa0;&#xa0;<span class="tag"><span class="ARCHIVE">ARCHIVE</span></span></h4>
</div>
</div>
<div id="outline-container-orga7297d9" class="outline-3">
<h3 id="orga7297d9"><span class="done DONE">DONE</span> Una nota puede tener un estado</h3>
<div class="outline-text-3" id="text-orga7297d9">
<p>
Puede configurar cualquier número de estados a utilizar: TODO, NEXT, DONE, etc.
</p>
</div>

<div id="outline-container-orgb18f030" class="outline-4">
<h4 id="orgb18f030">Hay dos <i>tipos</i> de estados: to-do y done</h4>
</div>
<div id="outline-container-orgbfdf442" class="outline-4">
<h4 id="orgbfdf442"><span class="done DONE">DONE</span> Esta es una nota con el tipo de estado <i>done</i></h4>
<div class="outline-text-4" id="text-orgbfdf442">
</div>
</div>
</div>

<div id="outline-container-org4d3b5bf" class="outline-3">
<h3 id="org4d3b5bf"><span class="done DONE">DONE</span> Una nota puede tener un momento de inicio programado</h3>
<div class="outline-text-3" id="text-org4d3b5bf">
</div>

<div id="outline-container-org1cb92f0" class="outline-4">
<h4 id="org1cb92f0">La fecha puede ser una repetición</h4>
<div class="outline-text-4" id="text-org1cb92f0">
</div>
</div>
</div>

<div id="outline-container-org3f12910" class="outline-3">
<h3 id="org3f12910"><span class="done DONE">DONE</span> También se puede establecer un tiempo límite</h3>
<div class="outline-text-3" id="text-org3f12910">
</div>
</div>

<div id="outline-container-org4fd9c75" class="outline-3">
<h3 id="org4fd9c75">Se soportan los recordatorios para horas programadas y de tiempo límite</h3>
<div class="outline-text-3" id="text-org4fd9c75">
<p>
Estan inhabilitadas por defecto y necesitan estar habilitadas en Configuración.
</p>
</div>
</div>

<div id="outline-container-orged9214a" class="outline-3">
<h3 id="orged9214a">Una nota puede tener una prioridad</h3>
<div class="outline-text-3" id="text-orged9214a">
<p>
Puedes cambiar el número de prioridades en Configuración. También puedes cambiar la prioridad por defecto - la prioridad asumida para notas sin una puesta.
</p>
</div>
</div>

<div id="outline-container-orgce070b4" class="outline-3">
<h3 id="orgce070b4">Una nota puede incluir enlaces</h3>
<div class="outline-text-3" id="text-orgce070b4">
<p>
Marcar un número de teléfono (tel:555-0199), enviar SMS (sms:555-0199), componer un correo electrónico (<a href="mailto:support@orgzly.com">mailto:support@orgzly.com</a>) o visitar una página web (<a href="http://www.orgzly.com">Orgzly.com</a>).
</p>

<p>
También puede enlazar a otra nota o bloc de notas dentro de la aplicación.
</p>

<p>
Consulte <a href="http://www.orgzly.com/help#links">http://www.orgzly.com/help#links</a> para obtener más información.
</p>
</div>
</div>

<div id="outline-container-org42a90e0" class="outline-3">
<h3 id="org42a90e0">Están soportados formatos tipográficos básicos</h3>
<div class="outline-text-3" id="text-org42a90e0">
<p>
Puede escribir palabras en <b>negrita</b>, <i>itálica</i>, <span class="underline">subrayadas</span>, <code>cita literal</code>, <code>código</code> y <del>tachadas</del>.
</p>
</div>
</div>

<div id="outline-container-org78b62f5" class="outline-3">
<h3 id="org78b62f5">Se pueden crear listas de verificación</h3>
<div class="outline-text-3" id="text-org78b62f5">
<ul class="org-ul">
<li class="on"><code>[X]</code> Tarea 1</li>
<li class="off"><code>[&#xa0;]</code> Tarea 2</li>
<li class="off"><code>[&#xa0;]</code> Tarea 3</li>
</ul>

<p>
Pulsa la caja de verificación para alternar su estado. Pulsa el botón de nueva línea al final de la línea para crear un nuevo elemento.
</p>
</div>
</div>
</div>

<div id="outline-container-org4fe33d0" class="outline-2">
<h2 id="org4fe33d0">Buscar</h2>
<div class="outline-text-2" id="text-org4fe33d0">
</div>
<div id="outline-container-orgc642fd2" class="outline-3">
<h3 id="orgc642fd2">Existen muchos operadores de búsqueda soportados</h3>
<div class="outline-text-3" id="text-orgc642fd2">
<p>
Puede buscar notas por estado, etiqueta, fecha de inicio, fecha límite, etc.
</p>

<p>
Visite <a href="http://www.orgzly.com/help#search">http://www.orgzly.com/help#search</a> para aprender más.
</p>
</div>
</div>

<div id="outline-container-org70fe488" class="outline-3">
<h3 id="org70fe488">Las consultas se pueden guardar como accesos rápidos</h3>
<div class="outline-text-3" id="text-org70fe488">
<p>
Pruebe los ejemplos de búsqueda del cajón de navegación y observe las consultas que utilizan.
</p>

<p>
Puede crear sus propias búsquedas y guardarlas haciendo clic en "Búsquedas" en el cajón de navegación.
</p>
</div>
</div>
</div>

<div id="outline-container-org4d1a7f5" class="outline-2">
<h2 id="org4d1a7f5">Sincronizando</h2>
<div class="outline-text-2" id="text-org4d1a7f5">
</div>
<div id="outline-container-org5f3f102" class="outline-3">
<h3 id="org5f3f102">Los cuadernos pueden guardarse como archivos de texto plano</h3>
<div class="outline-text-3" id="text-org5f3f102">
<p>
Los archivos se guardan en el formato de "Org mode".
</p>
</div>
</div>

<div id="outline-container-org50615a8" class="outline-3">
<h3 id="org50615a8">Tipo de ubicación (repositorio)</h3>
<div class="outline-text-3" id="text-org50615a8">
<p>
Puede guardar cuadernos sincronizados en su dispositivo móvil, tarjeta SD o en Dropbox.
</p>
</div>
</div>
</div>
<div class="taglist"></div>]]></description>
  <link>https://ea2fbmemacs.gitlab.io/posts2023-10-23-primeros-pasos-con-orgzly.html</link>
  <guid>https://ea2fbmemacs.gitlab.io/posts2023-10-23-primeros-pasos-con-orgzly.html</guid>
  <pubDate>Mon, 23 Oct 2023 15:30:00 +0200</pubDate>
</item>
<item>
  <title><![CDATA[Prueba de Estilo]]></title>
  <description><![CDATA[

<div id="outline-container-org2191d20" class="outline-2">
<h2 id="org2191d20">ESTO ES UNA PRUEBA DE ESTILO</h2>
<div class="outline-text-2" id="text-org2191d20">
</div>
<div id="outline-container-org40b2d77" class="outline-4">
<h4 id="org40b2d77">*eSTO ES una prueba de estilo</h4>
</div>
</div>
<div class="taglist"></div>]]></description>
  <link>https://ea2fbmemacs.gitlab.io/posts2023-10-23-prueba-de-estilo.html</link>
  <guid>https://ea2fbmemacs.gitlab.io/posts2023-10-23-prueba-de-estilo.html</guid>
  <pubDate>Mon, 23 Oct 2023 07:48:00 +0200</pubDate>
</item>
<item>
  <title><![CDATA[Calendario org agenda]]></title>
  <description><![CDATA[
<p>
Una de las maravillas de organizarte con Emacs, es la Agenda, es incleible cómo puedes controlar cualquier aspecto de tu flujo de trabajo diario con ella, sencilla y clara a la hora de mostrarte tu día a día.
</p>

<p>
Con calfw, vamos a poder ver una vista de calendario completa, en la cual se reflejen tanto nuestras citas, obligaciones como festividades o cualquier fecha relevante que creamo oportuna.
</p>

<p>
Lo llamamos con F5 y la vista quedaría así, reflejando todas nuestras citas y eventos progamados:
</p>


<p>
El código para Emacs sería el siguiente:
</p>

<p>
;;---------------------------------------------------------------------------
;;----------------------CALENDARIO------------------------------------------
;;--------------------------------------------------------------------------
(use-package calfw
 :ensure t
 :bind ([f5] . mi-calendario) ;; lo llamamos con F5
 :custom
 (cfw:org-overwrite-default-keybinding m)) ;; atajos de teclado de la agenda org-mode
; (setq cfw:display-calendar-holidays nil) ;; esconder fiestas calendario emacs
</p>

<p>
(use-package calfw-org
:ensure t)
</p>

<p>
;; calendarios a mostrar
(defun mi-calendario ()
  (interactive)
       (cfw:open-org-calendar))
       ; :contents-sources
       ; (list
        ; (cfw:org-create-source))))
</p>

<p>
;;first day week
(setq calendar-week-start-day 1) ; 0:Domingo, 1:Lunes
(setq calendar-holidays '((holiday-fixed 1 1 "Año Nuevo")
            (holiday-fixed 1 6 "Reyes Magos")
            (holiday-fixed 4 18 "Jueves Santo")
            (holiday-fixed 4 19 "Viernes Santo")
            (holiday-fixed 5 1 "Dia del Trabajador")
            (holiday-fixed 3 5 "Cinco Marzada")
            (holiday-fixed 1 29 "San Valero")
            (holiday-fixed 3 23 "San Chorche")
            (holiday-fixed 10 12 "Día del Pilar")
            (holiday-fixed 11 01 "Todos los Santos")
            (holiday-fixed 11 09 "Almudena")
            (holiday-fixed 12 06 "Constitución")
            (holiday-fixed 12 08 "Inmaculada")
            (holiday-fixed 12 25 "Navidad")
            ))
(setq calendar-week-start-day 1) ;; la semana empieza el lunes
(setq european-calendar-style t) ;; estilo europeo
</p>
<div class="taglist"></div>]]></description>
  <link>https://ea2fbmemacs.gitlab.io/posts2023-10-22-calendario-org-agenda.html</link>
  <guid>https://ea2fbmemacs.gitlab.io/posts2023-10-22-calendario-org-agenda.html</guid>
  <pubDate>Sun, 22 Oct 2023 18:14:00 +0200</pubDate>
</item>
<item>
  <title><![CDATA[Filtos]]></title>
  <description><![CDATA[
<nav id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#org1f8d0b3">Principal</a>
<ul>
<li><a href="#org64c9626">Secundario</a></li>
</ul>
</li>
</ul>
</div>
</nav>
<div id="outline-container-org1f8d0b3" class="outline-2">
<h2 id="org1f8d0b3">Principal</h2>
<div class="outline-text-2" id="text-org1f8d0b3">
</div>
<div id="outline-container-org64c9626" class="outline-3">
<h3 id="org64c9626">Secundario</h3>
</div>
</div>
<div class="taglist"></div>]]></description>
  <link>https://ea2fbmemacs.gitlab.io/posts2023-10-22-filtos.html</link>
  <guid>https://ea2fbmemacs.gitlab.io/posts2023-10-22-filtos.html</guid>
  <pubDate>Sun, 22 Oct 2023 08:34:00 +0200</pubDate>
</item>
</channel>
</rss>
